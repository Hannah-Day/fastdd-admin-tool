declare module "*.vue" {
    import Vue from "vue";
    export default Vue;
}

declare module "vuelidate";
declare module "vuelidate/*";
declare module "secure-ls/*";
