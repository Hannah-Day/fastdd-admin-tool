const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
//
// mix.js('resources/js/app.js', 'public/js')
//     .sass('resources/sass/app.scss', 'public/css');
mix.webpackConfig({
    resolve: {
        extensions: [".js", ".vue", ".json", ".ts"],
        alias: {
            "@mainStore": path.resolve(__dirname, "resources/js/store"),
            "@store": path.resolve(__dirname, "resources/js/store/modules"),
            "@queries": path.resolve(__dirname, "resources/js/apollo/queries"),
            "@mutations": path.resolve(
                __dirname,
                "resources/js/apollo/mutations"
            ),
            "@pages": path.resolve(__dirname, "resources/js/pages"),
            "@components": path.resolve(__dirname, "resources/js/components"),
            "@partials": path.resolve(
                __dirname,
                "resources/js/components/partials"
            ),
            "@plugins": path.resolve(__dirname, "resources/js/plugins"),
            "@mixins": path.resolve(__dirname, "resources/js/mixins"),
            "@includes": path.resolve(__dirname, "resources/js/includes")
            // '@application': path.resolve(__dirname, 'resources/js/components/_Application'),
            // '@auth': path.resolve(__dirname, 'resources/js/components/_Auth'),
            // '@pages': path.resolve(__dirname, 'resources/js/components/_Pages'),
            // '@templates': path.resolve(__dirname, 'resources/js/components/templates'),
            // '@helpers': path.resolve(__dirname, 'resources/js/helpers'),

            // '@routes': path.resolve(__dirname, 'resources/js/_routes'),
            // '@mutations': path.resolve(__dirname, 'resources/js/apollo/mutations'),
            // '@assets': path.resolve(__dirname, 'resources/sass'),
            // '@images': path.resolve(__dirname, 'resources/images')
        }
    }
})
    .ts("resources/js/app.ts", "public/js")
    .sass("resources/sass/app.scss", "public/css")
    .options({
        processCssUrls: false
    });
