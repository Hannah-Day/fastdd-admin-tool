import Vue from "vue";

// Import the main site page.
import SitePage from "@components/Site.vue";

// Import the components from Components index file.
import { Home } from "./../components";

const NotFound = Vue.extend({
    template: "<div>Page not found</div>"
});

// Export the components with the path and meta information.
export default [
    {
        path: "/",
        component: SitePage,
        children: [
            {
                path: "",
                components: Home,
                name: "home",
                meta: {
                    authentication: true,
                    needsAuth: false
                }
            }
        ]
    },
    { path: "/notFound", component: NotFound }
];
