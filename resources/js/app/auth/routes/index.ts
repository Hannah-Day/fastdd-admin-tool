// Import the main site page.
import AuthPage from "@components/Auth.vue";

// Import the components from Components index file.
import { Login } from "../components";

// Export the components with the path and meta information.
export default [
    {
        path: "/auth",
        component: AuthPage,
        children: [
            {
                path: "login",
                components: Login,
                name: "login",
                props: true,
                meta: {
                    authentication: true,
                    needsAuth: false
                }
            }
        ]
    }
];
