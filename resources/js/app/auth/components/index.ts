// Import vue.
import Vue from "vue";

// Export the components.
export const Login = Vue.component("login", require("./Login.vue"));
