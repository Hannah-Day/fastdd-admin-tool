// Import the module routes.
import site from "./site/routes";
import auth from "./auth/routes";
import application from "./application/routes";

// Export the routes.
export default [...site, ...auth, ...application];
