// Import vue and vue-router.
import Vue from "vue";
import Router from "vue-router";
import { routes } from "./../app/index";
import SecureLS from "secure-ls/dist/secure-ls.min"

// Init the use of vue-router.
Vue.use(Router);

// Set the router.
export const router = new Router({
    mode: "history",
    routes: routes
});

// Validate the user access to the pages.
router.beforeEach((to, from, next) => {
    if (!to.matched.length) {
        next("/notFound");
    } else {
        let ls = new SecureLS();

        let user = ls.get("user");

        if (user) {
            // User is logged in and tries to go to any guest page.
            if (to.meta.authentication) {
                // Send them to the main dashboard page.
                next({ name: "tasks" });
                return;
            }
            // Send the user to the intended page.
            next();
        } else {
            // User is not logged in and tries to go to any authorised page.
            if (to.meta.needsAuth) {
                // Send them to the login page.
                next({ name: "login" });
                return;
            }
            // Send the user to the intended page.
            next();
        }
    }
});

export default router;
